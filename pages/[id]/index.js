import React from "react";
import {
  getCoursesAndRouteWithRouteID,
  getUserRouteWithID,
} from "../../lib/firebase";
import ReactPlayer from "react-player";
import VideoPreviewCard from "../../components/card/VideoPreviewCard.component";
import Link from "next/link";

export const getServerSideProps = async ({ params }) => {
  const { id } = params;
  console.log("id: ", id);
  // let lectReviews;
  const user = await getUserRouteWithID(id);
  const routeNo = user.routeNo;

  let routes = await getCoursesAndRouteWithRouteID(routeNo);
  let roadmap = [];
  for (let j = 0; j < routes.roadmap.length; ++j) {
    for (let i = 0; i < routes.roadmapContent.length; ++i) {
      if (routes.roadmap[j] === routes.roadmapContent[i].id) {
        console.log(routes.roadmapContent[i]);
        roadmap.push(routes.roadmapContent[i]);
      }
    }
  }
  routes.roadmapContent = roadmap;
  // lectReviewRef=lectReview
  // if (lectDoc) {
  //   const lectReviewRef = lectDoc[0].ref
  //     .collection('lectReviews')
  //     .doc(slug as string);
  //   lectReviews = postToJSON(await lectReviewRef.get());
  // }
  // console.log('lectReviewDoc', lectReviewDoc);
  // if (lectDoc) {
  //   // const postRef = lectDoc.ref.collection('posts').doc(slug as string);
  //   post= postToJSON(await lectDoc.get());
  //   path = postRef.path;
  // }

  return {
    props: { id, routes, name: user.name, user },
    // revalidate: 5000, //re fetch after 5000ms
  };
};
const UserHomePage = (props) => {
  const { id, routes, name, user } = props;
  const checkCompletion = (courseID, contentNo) => {
    if (
      courseID === user.progress.course &&
      contentNo > parseInt(user.progress.content)
    ) {
      return false;
    }
    if (user.completedCourse.includes(courseID)) {
      return true;
    }
    if (
      courseID !== user.progress.course &&
      !user.completedCourse.includes(courseID)
    ) {
      return false;
    }
    console.log("courseID: ", courseID);
    console.log("user: ", user);
    return true;
  };
  return (
    // <main className="max-w-screen-xl md:mx-auto mx-10">
    <main className="md:mx-auto mx-10 flex flex-col my-5">
      <head>
        <title>{name} learnings</title>
      </head>
      {/* <p>Welcome Back!!!</p> */}
      <p className="text-3xl font-bold mx-auto">
        Your Personalized Financial Journey
      </p>
      <div className="mx-auto">
        {routes.roadmapContent.map((course) => {
          return (
            <>
              <h1 key={course.id} className="text-lg font-semibold">
                {course.courseName}
              </h1>
              <div key={course.id}>
                {course.content.map((content, index) => {
                  console.log("content: ", content);
                  return (
                    <a key={course.id} href={`/${id}/${course.id}/${index}`}>
                      <VideoPreviewCard
                        title={content.title}
                        done={checkCompletion(course.id, index)}
                        index={index}
                        courseId={course.id}
                        uid={id}
                      />
                    </a>
                  );
                })}
              </div>
            </>
          );
        })}
      </div>
    </main>
  );
};

export default UserHomePage;
