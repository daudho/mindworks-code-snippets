import React from "react";
import { useRouter } from "next/router";
import {
  firestore,
  getCourseContent,
  getUserRouteWithID,
} from "../../../lib/firebase";
import ReactPlayer from "react-player";
import Link from "next/link";
import ProgressBar from "@ramonak/react-progress-bar";

export async function getServerSideProps({ params }) {
  console.log("params: ", params);
  const user = await getUserRouteWithID(params.id);
  const courseContent = await getCourseContent(params.slug);
  console.log("courseCOntent: ", courseContent.content);
  return { props: { data: courseContent, user } };
}
const VideoContent = (props) => {
  const { user } = props;
  const { query } = useRouter();
  console.log("query: ", query);
  const [content, setContent] = React.useState(props.data);
  const checkCompletion = (courseID, contentNo) => {
    if (
      courseID === user.progress.course &&
      contentNo > parseInt(user.progress.content)
    ) {
      return false;
    }
    if (user.completedCourse.includes(user.progress.course)) {
      return true;
    }
    if (
      courseID !== user.progress.course &&
      !user.completedCourse.includes(user.progress.course)
    ) {
      return false;
    }
    return true;
  };
  const [complete, setComplete] = React.useState(
    checkCompletion(query.slug, query.count)
  );
  console.log("complete: ", complete);
  const addCompletion = async () => {
    let ref = await firestore.collection("users").doc(query.id);
    // const ref = await firestore.collection("users").doc(user.id);
    ref.update({
      progress: {
        course: query.slug,
        content: query.count,
      },
    });
  };
  return (
    <div className=" flex flex-col justify-center mx-10">
      <div className="mx-auto w-9/12 my-4">
        <h1 className="text-xl font-bold text-center mb-2">Progress</h1>
        <ProgressBar
          completed={(query.count / content.content.length) * 100}
          width="100%"
          height="20px"
          className="w-9/12"
        />
      </div>
      <br />
      <div className="shadow-lg rounded-lg p-5">
        <p className="text-lg font-bold text-center my-2">
          {content.content[parseInt(query.count)].title}
        </p>
        <ReactPlayer
          width="100%"
          height="100%"
          url={content.content[parseInt(query.count)].videoUrl}
        />
        <p className="text-center">
          {content.content[parseInt(query.count)].description}
        </p>
        <div className="flex justify-between">
          {query.count !== "0" && (
            <Link
              href={`/${query.id}/${query.slug}/${parseInt(query.count) - 1}`}
            >
              <button
                className="border border-indigo-500 hover:bg-indigo-600 focus:outline-none rounded-md px-6 py-3 mx-auto block text-indigo-500 hover:text-white transition duration-500 ease-in-out mt-10"
                disabled={query.count === "0"}
              >
                ←
              </button>
            </Link>
          )}
          {parseInt(query.count) != content.content.length - 1 ? (
            <Link
              href={`/${query.id}/${query.slug}/${parseInt(query.count) + 1}`}
            >
              <button
                className="bg-indigo-500 hover:bg-indigo-600 focus:outline-none rounded-md px-6 py-3 mx-auto block text-white transition duration-500 ease-in-out mt-10"
                onClick={async () => await addCompletion()}
              >
                →
              </button>
            </Link>
          ) : (
            <Link href={`/${query.id}/${query.slug}/summary`}>
              <button
                className="bg-indigo-500 hover:bg-indigo-600 focus:outline-none rounded-md px-6 py-3 mx-auto block text-white transition duration-500 ease-in-out mt-10"
                onClick={async () => await addCompletion()}
              >
                →
              </button>
            </Link>
          )}
        </div>
      </div>
    </div>
  );
};

export default VideoContent;
