import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { firebaseDB, firestore, getCourseContent } from "../../../lib/firebase";

export async function getServerSideProps({ params }) {
  const courseContent = await getCourseContent(params.slug);
  console.log("courseCOntent: ", courseContent.content);
  return { props: { data: courseContent } };
}

const Summary = (props) => {
  const { query } = useRouter();
  const [content, setContent] = React.useState(props.data);
  const addCompleted = async () => {
    let ref = await firestore.collection("users").doc(query.id);
    // const ref = await firestore.collection("users").doc(user.id);
    ref.update({
      completedCourse: firebaseDB.firestore.FieldValue.arrayUnion(query.slug),
    });
  };
  return (
    <div className="max-w-screen-xl mx-auto p-5">
      <h1 className="text-2xl text-center">{content.courseName}: Summary</h1>
      <p className="text-center">{content.summary}</p>
      <Link href="https://www.hlb.com.my/en/personal-banking/home.html">
        <button
          className="border border-indigo-500 hover:bg-indigo-600 focus:outline-none rounded-md px-6 py-3 mx-auto block text-indigo-500 hover:text-white transition duration-500 ease-in-out mt-10"
          disabled={query.count === "0"}
          onClick={async () => await addCompleted()}
        >
          Get your Rewards by starting your {content.category} journey with HLB
        </button>
      </Link>
      <Link href={`/${query.id}`}>
        <p
          className="text-gray-500 text-xs text-center underline mt-2"
          onClick={async () => await addCompleted()}
        >
          Skip
        </p>
      </Link>
    </div>
  );
};

export default Summary;
