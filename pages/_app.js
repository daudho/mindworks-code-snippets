import NavBar from "../components/Navbar.component";
import "../styles/globals.css";
import Image from "next/image";
import styles from "../styles/Home.module.css";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </head>
      <NavBar />
      <Component {...pageProps} className="max-w-screen-xl mx-auto" />
      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
          className="flex items-center content-center"
        >
          <p>Powered by </p> {/* <span className={styles.logo}> */}
          <Image
            src="/logo.png"
            alt="HLB Logo"
            width={120}
            height={30.195500002}
          />
          {/* </span> */}
        </a>
      </footer>
    </>
  );
}

export default MyApp;
