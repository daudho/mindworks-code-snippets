import React from "react";
import Image from "next/image";
import InputBox from "./InputBox.component";
import ProgressBar from "@ramonak/react-progress-bar";
import { Container, Row, Col } from "reactstrap";
import logo from "../../public/logo.png";
import { firestore, getMatchRoute } from "../../lib/firebase";
import { useRouter } from "next/router";
import "bootstrap/dist/css/bootstrap.min.css";

const OnBoardInfo = () => {
  const router = useRouter();
  const [page, setPage] = React.useState(1);
  const [state, setState] = React.useState({
    name: "",
  });
  const handleChange = async (key, val) => {
    await setState({
      ...state,
      [key]: val,
    });
    console.log("state: ", state);
  };
  const addUserAndAssignRoute = async () => {
    let tags = [];
    switch (state.marital) {
      case "Single":
        tags.push("single");
        break;
      case "Married":
        tags.push("married");
        break;
    }
    switch (state.finKnowledge) {
      case "None (Just Started)":
      case "I Know a Little":
        tags.push("beginner");
        break;
      case "Good":
        tags.push("intermediate");
        break;
      case "Very Good":
        tags.push("advanced");
        break;
    }
    tags.push(state.income);
    tags.push(state.finGoal);
    tags.push(state.risk);
    console.log("tags: ", tags);
    const routes = await getMatchRoute(tags);
    console.log(routes);
    if (routes === false) {
      alert(
        "Do not have suitable module for your now. Please make changes to your requirements."
      );
      return;
    }
    firestore
      .collection("users")
      .add({
        completedCourse: [],
        name: state.name,
        progress: {
          content: "",
          course: "",
        },
        routeNo: routes.id,
      })
      .then(function (docRef) {
        docRef.id;
        router.push(`/${docRef.id}`);
      });
  };
  return (
    <div className="max-w-screen-xl mx-auto px-5">
      <Container>
        <Row>
          <Col className="p-3 text-center">
            <Image alt="test" src={logo}></Image>
          </Col>
        </Row>
        <Row>
          <Col className="p-3 text-center mb-2">
            <p>Progress</p>
            <ProgressBar
              completed={((page - 1) / 4) * 100}
              width="100%"
              height="30px"
              className="w-9/12"
            />
          </Col>
        </Row>
      </Container>
      <div className={`${page == 1 ? "" : "hidden"}`}>
        <InputBox
          label={"Name"}
          placeholder="John Doe"
          state={state}
          handleChange={handleChange}
        />
        <div className="flex flex-wrap -mx-3 mb-2">
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-state"
            >
              Gender
            </label>
            <div className="relative">
              <select
                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="grid-state"
                value={state.gender}
                onChange={async (e) =>
                  await handleChange("gender", e.target.value)
                }
              >
                <option disabled>Gender</option>
                <option>Male</option>
                <option>Female</option>
                <option>Other</option>
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg
                  className="fill-current h-4 w-4"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
              </div>
            </div>
          </div>
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="age"
            >
              Age
            </label>
            <input
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="age"
              type="number"
              placeholder="18"
              value={state.age}
              onChange={async (e) => await handleChange("age", e.target.value)}
            />
          </div>
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="country"
            >
              Country
            </label>
            <div className="relative">
              <select
                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="country"
                onChange={async (e) =>
                  await handleChange("country", e.target.value)
                }
              >
                <option>Brunei</option>
                <option>Cambodia</option>
                <option>Indonesia</option>
                <option>Laos</option>
                <option>Malaysia</option>
                <option>Myanmar</option>
                <option>Philippines</option>
                <option>Singapore</option>
                <option>Thailand</option>
                <option>Vietnam</option>
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg
                  className="fill-current h-4 w-4"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 my-6">
          <div className="w-full md:w-2/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="country"
            >
              Income Range (include all source of income)
            </label>
            <div className="relative">
              <select
                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="income"
                onChange={async (e) =>
                  await handleChange("income", e.target.value)
                }
              >
                <option value="low-income">60,000 - 99,999 MYR/year</option>
                <option value="mid-income">100,000 - 149,000 MYR/year</option>
                <option value="mid-income">150,000 - 199,000 MYR/year</option>
                <option value="high-income">200,000 or more MYR/year</option>
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg
                  className="fill-current h-4 w-4"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Part 2: Marital Status */}
      <div className={`${page == 2 ? "" : "hidden"}`}>
        <div className="flex flex-wrap -mx-3 my-6">
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="country"
            >
              Marital Status
            </label>
            <div className="relative">
              <select
                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                id="marital"
                onChange={async (e) =>
                  await handleChange("marital", e.target.value)
                }
              >
                <option>Single</option>
                <option>Married</option>
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg
                  className="fill-current h-4 w-4"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
              </div>
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="country"
            >
              Number of children
            </label>
            <input
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="age"
              type="number"
              placeholder="2"
              onChange={async (e) =>
                await handleChange("childNo", e.target.value)
              }
            />
          </div>
        </div>
      </div>
      {/* Part 3: General Questions */}
      {/* className={`${page == 1 ? "" : "hidden"} flex flex-wrap -mx-3 my-6`} */}
      <div className={`${page == 3 ? "" : "hidden"} flex flex-wrap -mx-3 my-6`}>
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="country"
          >
            Rate your financial knowledge
          </label>
          <div className="relative">
            <select
              className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="finKnowledge"
              onChange={async (e) =>
                await handleChange("finKnowledge", e.target.value)
              }
            >
              <option>Very Good</option>
              <option>Good</option>
              <option>I Know a Little</option>
              <option>None (Just Started)</option>
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
              <svg
                className="fill-current h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </div>
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="country"
          >
            What is your financial goal?
          </label>
          <div className="relative">
            <select
              className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="finGoal"
              onChange={async (e) =>
                await handleChange("finGoal", e.target.value)
              }
            >
              <option value="invest">Learn to Invest</option>
              <option value="earn">Earn Extra Income</option>
              <option value="retire">Prepare Retirement Funds</option>
              <option value="pfinance">Learn Personal Finance</option>
              <option value="insurance">Choosing Insurance</option>
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
              <svg
                className="fill-current h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </div>
      </div>
      <div className={`${page == 4 ? "" : "hidden"} flex flex-wrap -mx-3 my-6`}>
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="risk"
          >
            Investment Risk tolerance
          </label>
          <div className="relative">
            <select
              className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="risk"
              onChange={async (e) => await handleChange("risk", e.target.value)}
            >
              <option value="low-risk">Minimal</option>
              <option value="low-risk">Low</option>
              <option value="medium-risk">Medium</option>
              <option value="high-risk">High</option>
              <option value="high-risk">Very High</option>
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
              <svg
                className="fill-current h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </div>

        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="age"
          >
            How many housing & car loans do you are have?
          </label>
          <input
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            id="age"
            type="number"
            placeholder="2"
            onChange={async (e) => await handleChange("loanNo", e.target.value)}
          />
        </div>
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="country"
          >
            Estimated total monthly loan repayment
          </label>
          <div className="relative">
            <select
              className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="country"
              onChange={async (e) =>
                await handleChange("loanPayment", e.target.value)
              }
            >
              <option>500</option>
              <option>1000</option>
              <option>2000</option>
              <option>3000</option>
              <option>10000</option>
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
              <svg
                className="fill-current h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
        </div>
      </div>

      <div className="flex justify-between">
        {page > 1 && (
          <button
            className="bg-indigo-400 hover:bg-indigo-600 focus:outline-none rounded-md w-full py-3 mx-auto block text-white transition duration-500 ease-in-out mt-2"
            onClick={() => setPage(page - 1)}
          >
            Prev
          </button>
        )}
        {page < 4 ? (
          <button
            className="bg-indigo-500 hover:bg-indigo-600 focus:outline-none rounded-md w-full py-3 mx-auto block text-white transition duration-500 ease-in-out mt-2"
            onClick={() => setPage(page + 1)}
          >
            Next
          </button>
        ) : (
          <button
            className="bg-green-500 hover:bg-green-600 focus:outline-none rounded-md w-full py-3 mx-auto block text-white transition duration-500 ease-in-out mt-2"
            onClick={() => addUserAndAssignRoute()}
          >
            Complete
          </button>
        )}
      </div>
    </div>
  );
};

export default OnBoardInfo;
