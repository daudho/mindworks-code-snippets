import React from "react";

const InputBox = ({ label, placeholder, handleChange, state }) => {
  return (
    <div className="flex flex-wrap -mx-3 mb-auto">
      <div className="w-full md:w-2/2 px-3 mb-2 md:mb-0">
        <label
          className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
          htmlFor="grid-first-name"
        >
          First Name
        </label>
        <input
          className="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
          id="grid-first-name"
          type="text"
          value={state.name}
          placeholder={placeholder}
          onChange={(e) => handleChange("name", e.target.value)}
        />
        {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
      </div>
    </div>
  );
};

export default InputBox;
