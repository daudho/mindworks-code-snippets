import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Image from "next/image";
import logo from "../public/logo.png";
const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md" className="p-2">
        <NavbarBrand href="/">
          <Image
            alt="test"
            src={logo}
            width="120px"
            height="30.195500002px"
          ></Image>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar></Nav>
          <NavbarText href="/wKhbxeW5bVegXiqVgVBb">Demo</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default NavBar;
