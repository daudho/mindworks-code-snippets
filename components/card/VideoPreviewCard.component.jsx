import React from "react";
import Image from "next/image";
import Link from "next/link";

const VideoPreviewCard = ({ title, index, done, courseId, uid }) => {
  return (
    <div className="my-2">
      <div className="bg-white p-4 shadow-xl rounded-lg max-w-md flex flex-row items-center transition transform duration-500 ease-in-out hover:-translate-y-2">
        <Image src="/play.svg" alt="Vercel Logo" width={24} height={24} />
        <div className=" ml-4">
          <Image
            src="/previewImg.png"
            alt="PreviewImage"
            width={100}
            height={68.749999979}
          />
        </div>
        <div className="flex flex-col mx-4 gap-y-1">
          <p className="text-base ">{title}</p>
          {/* <p className="text-sm truncate">{description}</p> */}
          <p className="text-gray-300 text-xs flex gap-x-0.5	">
            <Image src="/time.svg" alt="Time" width={16} height={16} /> {"    "}
            01:30
          </p>
        </div>
        <div className="flex-grow h-16 ..."></div>
        {done ? (
          <Image
            src="/tick-circle.svg"
            className="float-right"
            alt="tick"
            width={32}
            height={32}
          />
        ) : (
          <Image
            src="/otw.svg"
            className="float-right"
            alt="tick"
            width={32}
            height={32}
          />
        )}
      </div>
    </div>
  );
};

export default VideoPreviewCard;
