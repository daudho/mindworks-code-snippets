import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const clientCredentials = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
  authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
  storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.NEXT_PUBLIC_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.NEXT_PUBLIC_FIREBASE_APP_ID,
  measurementId: process.env.NEXT_PUBLIC_FIREBASE_MEASUREMENT_ID,
};

if (!firebase.apps.length) {
  firebase.initializeApp(clientCredentials);
}
export const auth = firebase.auth();
export const storage = firebase.storage();
export const firestore = firebase.firestore();
export const STATE_CHANGED = firebase.storage.TaskEvent.STATE_CHANGED;
export const fromMillis = firebase.firestore.Timestamp.fromMillis;
export const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp;
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const postToJSON = (doc) => {
  const data = doc.data();
  return {
    ...data,
    // createdAt: data.createdAt.toMillis(),
    // updatedAt: data.updatedAt.toMillis(),
  };
};
export const getUserRouteWithID = async (id) => {
  const userRef = firestore.collection("users").doc(id);
  // const query = moduleRef.where('courseID', "==", courseID).limit(1);
  const userDoc = postToJSON(await userRef.get());
  return userDoc;
};
export const getCoursesAndRouteWithRouteID = async (routeID) => {
  const routesRef = firestore.collection("routes").doc(routeID);
  let routesDoc = (await routesRef.get()).data();
  const roadMaps = routesDoc?.roadmap;
  const courseRef = firestore.collection("courses");
  const query = courseRef.where("id", "in", roadMaps);
  const courseDoc = (await query.get()).docs.map(postToJSON);
  routesDoc.roadmapContent = courseDoc;
  return routesDoc || [];
};
export const getCourseContent = async (slug) => {
  const courseRef = firestore.collection("courses").doc(slug);
  const courseDoc = postToJSON(await courseRef.get());
  return courseDoc;
};
export const getMatchRoute = async (tags) => {
  const routesRef = firestore.collection("routes");
  let query = routesRef;
  // const query = routesRef.where("tags", "array-contains-any", tags);
  tags.map((tag) => {
    query = query.where(`tags.${tag}`, "==", true);
    return true;
  });
  const routesDoc = (await query.get()).docs.map(postToJSON);
  return routesDoc[0] || false;
};
export const firebaseDB = firebase;
